/* jshint node: true */
'use strict';

module.exports = {
  name: 'ember-cli-proj4',

  included: function(app) {
   this._super.included(app);
   this.app.import(app.bowerDirectory + '/proj4/dist/proj4-src.js');
  }
};
